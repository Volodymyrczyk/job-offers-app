package com.junioroffers.infrastructure.offer.client;

import com.junioroffers.infrastructure.RemoteOfferClient;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class OfferHttpClientTest implements SampleRestTemplateExchangeResponse, SampleOfferResponse {

    @Test
    public void should_return_one_element_list_of_offers() {
        //given
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<JobOfferDto>> responseEntity = responseWithOneOffer();
        when(getExchange(restTemplate))
                .thenReturn(responseEntity);
        RemoteOfferClient remoteHttpClient = new OfferHttpClient(restTemplate,"http://programming-masterpiece.com",1123);

        //when
        List<JobOfferDto> offers = remoteHttpClient.getOffers();
        //then
        assertThat(offers.size()).isEqualTo(1);
    }

    @Test
    public void should_return_empty_list_of_offers() {
        // given
        final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<JobOfferDto>> responseEntity = responseWithNoOffers();
        when(getExchange(restTemplate))
                .thenReturn(responseEntity);
        RemoteOfferClient remoteOfferClient = new OfferHttpClient(restTemplate, "http://programming-masterpiece.com", 1123);

        // when
        final List<JobOfferDto> offers = remoteOfferClient.getOffers();

        // then
        AssertionsForClassTypes.assertThat(offers.size()).isEqualTo(0);
    }

    @Test
    public void should_return_two_offers() {
        // given
        final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<JobOfferDto>> responseEntity = responseWithOffers(emptyOffer(), emptyOffer());
        when(getExchange(restTemplate))
                .thenReturn(responseEntity);
        RemoteOfferClient remoteOfferClient = new OfferHttpClient(restTemplate, "http://programming-masterpiece.com", 1123);

        // when
        final List<JobOfferDto> offers = remoteOfferClient.getOffers();

        // then
        AssertionsForClassTypes.assertThat(offers.size()).isEqualTo(2);
    }

}
